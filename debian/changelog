qt6-speech (6.8.2-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 09 Feb 2025 09:33:54 +1000

qt6-speech (6.8.1-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 08 Dec 2024 07:32:05 +1000

qt6-speech (6.8.0-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Wed, 24 Jul 2024 21:18:40 +1000

qt6-speech (6.7.0-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sat, 17 Feb 2024 01:04:22 +1000

qt6-speech (6.6.1-0neon) jammy; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New release

  [ Carlos De Maine ]
  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 10 Dec 2023 16:21:28 +1000

qt6-speech (6.4.2-3~bpo11+1) bullseye-backports; urgency=medium

  * Rebuild for bullseye-backports.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 11 Mar 2023 13:16:29 -0300

qt6-speech (6.4.2-3) unstable; urgency=medium

  * Team upload.
  * Do not install plugins-related CMake files (Closes: #1031078).
    Thanks Jonas Smedegaard for the bug report.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 11 Feb 2023 17:22:59 -0300

qt6-speech (6.4.2-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Add missing B-D qml6-module-qtquick3d-spatialaudio (Closes:
    #1030471).

 -- Patrick Franz <deltaone@debian.org>  Sat, 04 Feb 2023 11:13:21 +0100

qt6-speech (6.4.2-1~bpo11+1) bullseye-backports; urgency=medium

  * Rebuild for bullseye-backports.
  * Add myself as a backports uploader.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Thu, 26 Jan 2023 16:04:21 -0300

qt6-speech (6.4.2-1) unstable; urgency=medium

  [ Patrick Franz ]
  * Switch to the official 6.4.2 tarball, the tarball is the same.
  * Enable link time optimization.

 -- Patrick Franz <deltaone@debian.org>  Sun, 22 Jan 2023 11:33:34 +0100

qt6-speech (6.4.2~rc1-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Fri, 30 Dec 2022 16:59:03 +0100

qt6-speech (6.4.2~rc1-1) experimental; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New upstream release.
    - Bump Qt build dependencies.
  * Bump standards version to 4.6.2, no changes required.

 -- Patrick Franz <deltaone@debian.org>  Tue, 27 Dec 2022 22:27:41 +0100

qt6-speech (6.4.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.4.1).
  * Bump Qt B-Ds to 6.4.1.

 -- Patrick Franz <deltaone@debian.org>  Thu, 17 Nov 2022 20:55:49 +0100

qt6-speech (6.4.0-1) experimental; urgency=medium

  * Initial release. (Closes: #1021682)

 -- Patrick Franz <deltaone@debian.org>  Sat, 22 Oct 2022 17:12:10 +0200
